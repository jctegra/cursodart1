import 'cart.dart';
import 'item.dart';
import 'product.dart';

main() {
  Cart c = new Cart(
    itens: [
      Item(
          product: Product(
            name: "Lápis",
            price: 5.99,
            discount: 0.3,
          ),
          quantity: 2),
      Item(
        product: Product(
          name: "Borracha",
          price: 2.99,
          discount: 0.1,
        ),
        quantity: 5,
      )
    ],
  );

  print(c.listItens);
}
