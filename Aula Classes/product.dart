class Product {
  late double _price, _discount;
  late String _name;

  Product(
      {required String name, required double price, required double discount}) {
    this._name = name;
    this._discount = discount;
    this._price = price;
  }

  double get priceWithDiscount {
    return (1 - _discount) * _price;
  }

  toString() {
    return _name;
  }
}
