import 'product.dart';

class Item {
  late Product product;
  late int _quantity;

  Item({required Product product, required int quantity}) {
    this.product = product;
    this._quantity = quantity;
  }

  int get quantity => _quantity;

  double get total {
    return product.priceWithDiscount * _quantity;
  }

  toString() {
    return '${product.toString()} x $quantity = ${total.toStringAsFixed(2)}';
  }
}
