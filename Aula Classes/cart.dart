import 'item.dart';

class Cart {
  late List<Item> itens;

  Cart({required this.itens});

  String get listItens {
    String data = "";
    for (Item item in itens) {
      data += item.toString() + "\n";
    }
    return data;
  }
}
