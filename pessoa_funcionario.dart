class Pessoa {
  String name, cpf;

  Pessoa(this.name, this.cpf);

  String nomeCPF() {
    return "$name - $cpf";
  }
}

class Funcionario extends Pessoa {
  String cargo;

  Funcionario(String name, String cpf, this.cargo) : super(name, cpf);

  String nomeCPF() {
    return "$name - $cpf - $cargo";
  }
}

main() {
  Funcionario f = new Funcionario("João", "123.456.789-00", "Programador");
  print(f.nomeCPF());
}
