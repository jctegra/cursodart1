int soma(int n1, [var n2]) {
  if (n2 != null) {
    return n1 + (n2 as int);
  }
  return n1;
}

double divisao(double n1, double n2) {
  return n1 / n2;
}

main() {
  print(soma(10, 30));
  print(soma(30));
  print(divisao(10, 2));
}
