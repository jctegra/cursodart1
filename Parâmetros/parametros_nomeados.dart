// String nomeCompleto({String nome, String sobrenome}) {
//   return '$nome $sobrenome';
// }

// String nomeCompleto({String nome = "Carlos", String sobrenome = "Pereira"}) {
//   return '$nome $sobrenome';
// }

String nomeCompleto({required String nome, required String sobrenome}) {
  return '$nome $sobrenome';
}

main() {
  print(nomeCompleto(
    sobrenome: 'Silva',
    nome: 'João',
  ));
  // print(nomeCompleto());
}
